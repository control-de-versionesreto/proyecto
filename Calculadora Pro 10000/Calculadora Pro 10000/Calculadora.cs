﻿using Calculadora_Pro_10000.Clases;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculadora_Pro_10000
{
    public partial class Calculadora : Form
    {
        Double primero;
        Double segundo;
        Double resultado;
        String operador = "";
        
        public Calculadora()
        {
            InitializeComponent();
        }

        Clases.ClsSuma suma = new Clases.ClsSuma();
        Clases.ClsMult mult = new Clases.ClsMult();
        Clases.ClsDivision div = new Clases.ClsDivision();
        Clases.ClsResta resta = new Clases.ClsResta();

        private void textscreen_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void butt0_Click(object sender, EventArgs e)
        {
            textscreen.Text = textscreen.Text + "0"; 
        }

        private void butt1_Click(object sender, EventArgs e)
        {
            textscreen.Text = textscreen.Text + "1";
        }

        private void butt2_Click(object sender, EventArgs e)
        {
            textscreen.Text = textscreen.Text + "2";
        }

        private void butt3_Click(object sender, EventArgs e)
        {
            textscreen.Text = textscreen.Text + "3";
        }

        private void butt4_Click(object sender, EventArgs e)
        {
            textscreen.Text = textscreen.Text + "4";
        }

        private void butt5_Click(object sender, EventArgs e)
        {
            textscreen.Text = textscreen.Text + "5";
        }

        private void butt6_Click(object sender, EventArgs e)
        {
            textscreen.Text = textscreen.Text + "6";
        }

        private void butt7_Click(object sender, EventArgs e)
        {
            textscreen.Text = textscreen.Text + "7";
        }

        private void butt8_Click(object sender, EventArgs e)
        {
            textscreen.Text = textscreen.Text + "8";
        }

        private void butt9_Click(object sender, EventArgs e)
        {
            textscreen.Text = textscreen.Text + "9";
        }

        private void buttsuma_Click(object sender, EventArgs e)
        {
            operador = "+";
            primero = double.Parse(textscreen.Text);
            textscreen.Clear();
        }

        private void buttresta_Click(object sender, EventArgs e)
        {
            operador = "-";
            primero = double.Parse(textscreen.Text);
            textscreen.Clear();
        }

        private void buttpunto_Click(object sender, EventArgs e)
        {
            textscreen.Text = textscreen.Text + ".";
        }

        private void buttigual_Click(object sender, EventArgs e)
        {
            segundo = double.Parse(textscreen.Text);
            double Sum;
            double Res;
            double Mult;
            double Div;

            switch (operador)
            {
                case "+":
                    Sum = suma.Sumar((primero), (segundo));
                    textscreen.Text = Sum.ToString();
                    break;

                case "-":
                    Res = resta.Restar((primero), (segundo));
                    textscreen.Text = Res.ToString();
                    break;

                case "*":
                    Mult = mult.Multiplicar((primero), (segundo));
                    textscreen.Text = Mult.ToString();
                    break;

                case "/":
                    Div = div.Division((primero), (segundo));
                    textscreen.Text = Div.ToString();
                    break;


            }

        }

        private void buttborrar_Click(object sender, EventArgs e)
        {
            if (textscreen.Text.Length == 1)
                textscreen.Text = "";
            else
                textscreen.Text = textscreen.Text.Substring(0, textscreen.Text.Length - 1);
        }

        private void buttinicio_Click(object sender, EventArgs e)
        {
            textscreen.Clear();
        }

        private void buttmult_Click(object sender, EventArgs e)
        {
            operador = "*";
            primero = double.Parse(textscreen.Text);
            textscreen.Clear();
        }

        private void buttdivi_Click(object sender, EventArgs e)
        {
            operador = "/";
            primero = double.Parse(textscreen.Text);
            textscreen.Clear();
        }
    }
}
