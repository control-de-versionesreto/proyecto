﻿namespace Calculadora_Pro_10000
{
    partial class Calculadora
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Calculadora));
            this.buttsuma = new System.Windows.Forms.Button();
            this.butt7 = new System.Windows.Forms.Button();
            this.butt4 = new System.Windows.Forms.Button();
            this.butt1 = new System.Windows.Forms.Button();
            this.butt0 = new System.Windows.Forms.Button();
            this.butt2 = new System.Windows.Forms.Button();
            this.butt5 = new System.Windows.Forms.Button();
            this.butt8 = new System.Windows.Forms.Button();
            this.buttresta = new System.Windows.Forms.Button();
            this.buttigual = new System.Windows.Forms.Button();
            this.buttborrar = new System.Windows.Forms.Button();
            this.buttinicio = new System.Windows.Forms.Button();
            this.buttdivi = new System.Windows.Forms.Button();
            this.buttpunto = new System.Windows.Forms.Button();
            this.butt3 = new System.Windows.Forms.Button();
            this.butt6 = new System.Windows.Forms.Button();
            this.butt9 = new System.Windows.Forms.Button();
            this.buttmult = new System.Windows.Forms.Button();
            this.textscreen = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // buttsuma
            // 
            this.buttsuma.BackColor = System.Drawing.Color.MediumPurple;
            this.buttsuma.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttsuma.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttsuma.Location = new System.Drawing.Point(9, 60);
            this.buttsuma.Name = "buttsuma";
            this.buttsuma.Size = new System.Drawing.Size(59, 54);
            this.buttsuma.TabIndex = 0;
            this.buttsuma.Text = "+";
            this.buttsuma.UseVisualStyleBackColor = false;
            this.buttsuma.Click += new System.EventHandler(this.buttsuma_Click);
            // 
            // butt7
            // 
            this.butt7.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.butt7.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.butt7.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butt7.Location = new System.Drawing.Point(9, 121);
            this.butt7.Name = "butt7";
            this.butt7.Size = new System.Drawing.Size(59, 54);
            this.butt7.TabIndex = 1;
            this.butt7.Text = "7";
            this.butt7.UseVisualStyleBackColor = false;
            this.butt7.Click += new System.EventHandler(this.butt7_Click);
            // 
            // butt4
            // 
            this.butt4.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.butt4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.butt4.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butt4.Location = new System.Drawing.Point(9, 181);
            this.butt4.Name = "butt4";
            this.butt4.Size = new System.Drawing.Size(59, 54);
            this.butt4.TabIndex = 2;
            this.butt4.Text = "4";
            this.butt4.UseVisualStyleBackColor = false;
            this.butt4.Click += new System.EventHandler(this.butt4_Click);
            // 
            // butt1
            // 
            this.butt1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.butt1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.butt1.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butt1.Location = new System.Drawing.Point(9, 241);
            this.butt1.Name = "butt1";
            this.butt1.Size = new System.Drawing.Size(59, 54);
            this.butt1.TabIndex = 3;
            this.butt1.Text = "1";
            this.butt1.UseVisualStyleBackColor = false;
            this.butt1.Click += new System.EventHandler(this.butt1_Click);
            // 
            // butt0
            // 
            this.butt0.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.butt0.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.butt0.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butt0.Location = new System.Drawing.Point(9, 301);
            this.butt0.Name = "butt0";
            this.butt0.Size = new System.Drawing.Size(124, 54);
            this.butt0.TabIndex = 4;
            this.butt0.Text = "0";
            this.butt0.UseVisualStyleBackColor = false;
            this.butt0.Click += new System.EventHandler(this.butt0_Click);
            // 
            // butt2
            // 
            this.butt2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.butt2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.butt2.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butt2.Location = new System.Drawing.Point(74, 241);
            this.butt2.Name = "butt2";
            this.butt2.Size = new System.Drawing.Size(59, 54);
            this.butt2.TabIndex = 8;
            this.butt2.Text = "2";
            this.butt2.UseVisualStyleBackColor = false;
            this.butt2.Click += new System.EventHandler(this.butt2_Click);
            // 
            // butt5
            // 
            this.butt5.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.butt5.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.butt5.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butt5.Location = new System.Drawing.Point(74, 181);
            this.butt5.Name = "butt5";
            this.butt5.Size = new System.Drawing.Size(59, 54);
            this.butt5.TabIndex = 7;
            this.butt5.Text = "5";
            this.butt5.UseVisualStyleBackColor = false;
            this.butt5.Click += new System.EventHandler(this.butt5_Click);
            // 
            // butt8
            // 
            this.butt8.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.butt8.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.butt8.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butt8.Location = new System.Drawing.Point(74, 121);
            this.butt8.Name = "butt8";
            this.butt8.Size = new System.Drawing.Size(59, 54);
            this.butt8.TabIndex = 6;
            this.butt8.Text = "8";
            this.butt8.UseVisualStyleBackColor = false;
            this.butt8.Click += new System.EventHandler(this.butt8_Click);
            // 
            // buttresta
            // 
            this.buttresta.BackColor = System.Drawing.Color.MediumPurple;
            this.buttresta.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttresta.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttresta.Location = new System.Drawing.Point(74, 60);
            this.buttresta.Name = "buttresta";
            this.buttresta.Size = new System.Drawing.Size(59, 54);
            this.buttresta.TabIndex = 5;
            this.buttresta.Text = "-";
            this.buttresta.UseVisualStyleBackColor = false;
            this.buttresta.Click += new System.EventHandler(this.buttresta_Click);
            // 
            // buttigual
            // 
            this.buttigual.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttigual.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttigual.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttigual.Location = new System.Drawing.Point(204, 241);
            this.buttigual.Name = "buttigual";
            this.buttigual.Size = new System.Drawing.Size(59, 114);
            this.buttigual.TabIndex = 14;
            this.buttigual.Text = "=";
            this.buttigual.UseVisualStyleBackColor = false;
            this.buttigual.Click += new System.EventHandler(this.buttigual_Click);
            // 
            // buttborrar
            // 
            this.buttborrar.BackColor = System.Drawing.Color.Tomato;
            this.buttborrar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttborrar.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttborrar.Location = new System.Drawing.Point(204, 181);
            this.buttborrar.Name = "buttborrar";
            this.buttborrar.Size = new System.Drawing.Size(59, 54);
            this.buttborrar.TabIndex = 12;
            this.buttborrar.Text = "<";
            this.buttborrar.UseVisualStyleBackColor = false;
            this.buttborrar.Click += new System.EventHandler(this.buttborrar_Click);
            // 
            // buttinicio
            // 
            this.buttinicio.BackColor = System.Drawing.Color.Tomato;
            this.buttinicio.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttinicio.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttinicio.Location = new System.Drawing.Point(204, 121);
            this.buttinicio.Name = "buttinicio";
            this.buttinicio.Size = new System.Drawing.Size(59, 54);
            this.buttinicio.TabIndex = 11;
            this.buttinicio.Text = "CE";
            this.buttinicio.UseVisualStyleBackColor = false;
            this.buttinicio.Click += new System.EventHandler(this.buttinicio_Click);
            // 
            // buttdivi
            // 
            this.buttdivi.BackColor = System.Drawing.Color.MediumPurple;
            this.buttdivi.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttdivi.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttdivi.Location = new System.Drawing.Point(204, 61);
            this.buttdivi.Name = "buttdivi";
            this.buttdivi.Size = new System.Drawing.Size(59, 54);
            this.buttdivi.TabIndex = 10;
            this.buttdivi.Text = "/";
            this.buttdivi.UseVisualStyleBackColor = false;
            this.buttdivi.Click += new System.EventHandler(this.buttdivi_Click);
            // 
            // buttpunto
            // 
            this.buttpunto.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttpunto.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttpunto.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttpunto.Location = new System.Drawing.Point(139, 301);
            this.buttpunto.Name = "buttpunto";
            this.buttpunto.Size = new System.Drawing.Size(59, 54);
            this.buttpunto.TabIndex = 19;
            this.buttpunto.Text = ".";
            this.buttpunto.UseVisualStyleBackColor = false;
            this.buttpunto.Click += new System.EventHandler(this.buttpunto_Click);
            // 
            // butt3
            // 
            this.butt3.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.butt3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.butt3.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butt3.Location = new System.Drawing.Point(139, 241);
            this.butt3.Name = "butt3";
            this.butt3.Size = new System.Drawing.Size(59, 54);
            this.butt3.TabIndex = 18;
            this.butt3.Text = "3";
            this.butt3.UseVisualStyleBackColor = false;
            this.butt3.Click += new System.EventHandler(this.butt3_Click);
            // 
            // butt6
            // 
            this.butt6.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.butt6.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.butt6.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butt6.Location = new System.Drawing.Point(139, 181);
            this.butt6.Name = "butt6";
            this.butt6.Size = new System.Drawing.Size(59, 54);
            this.butt6.TabIndex = 17;
            this.butt6.Text = "6";
            this.butt6.UseVisualStyleBackColor = false;
            this.butt6.Click += new System.EventHandler(this.butt6_Click);
            // 
            // butt9
            // 
            this.butt9.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.butt9.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.butt9.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butt9.Location = new System.Drawing.Point(139, 121);
            this.butt9.Name = "butt9";
            this.butt9.Size = new System.Drawing.Size(59, 54);
            this.butt9.TabIndex = 16;
            this.butt9.Text = "9";
            this.butt9.UseVisualStyleBackColor = false;
            this.butt9.Click += new System.EventHandler(this.butt9_Click);
            // 
            // buttmult
            // 
            this.buttmult.BackColor = System.Drawing.Color.MediumPurple;
            this.buttmult.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttmult.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttmult.Location = new System.Drawing.Point(139, 60);
            this.buttmult.Name = "buttmult";
            this.buttmult.Size = new System.Drawing.Size(59, 54);
            this.buttmult.TabIndex = 15;
            this.buttmult.Text = "*";
            this.buttmult.UseVisualStyleBackColor = false;
            this.buttmult.Click += new System.EventHandler(this.buttmult_Click);
            // 
            // textscreen
            // 
            this.textscreen.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.textscreen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textscreen.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textscreen.Location = new System.Drawing.Point(9, 5);
            this.textscreen.Name = "textscreen";
            this.textscreen.ReadOnly = true;
            this.textscreen.Size = new System.Drawing.Size(254, 49);
            this.textscreen.TabIndex = 20;
            this.textscreen.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textscreen.TextChanged += new System.EventHandler(this.textscreen_TextChanged);
            // 
            // Calculadora
            // 
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.BackgroundImage = global::Calculadora_Pro_10000.Properties.Resources.XD;
            this.ClientSize = new System.Drawing.Size(271, 364);
            this.Controls.Add(this.textscreen);
            this.Controls.Add(this.buttpunto);
            this.Controls.Add(this.butt3);
            this.Controls.Add(this.butt6);
            this.Controls.Add(this.butt9);
            this.Controls.Add(this.buttmult);
            this.Controls.Add(this.buttigual);
            this.Controls.Add(this.buttborrar);
            this.Controls.Add(this.buttinicio);
            this.Controls.Add(this.buttdivi);
            this.Controls.Add(this.butt2);
            this.Controls.Add(this.butt5);
            this.Controls.Add(this.butt8);
            this.Controls.Add(this.buttresta);
            this.Controls.Add(this.butt0);
            this.Controls.Add(this.butt1);
            this.Controls.Add(this.butt4);
            this.Controls.Add(this.butt7);
            this.Controls.Add(this.buttsuma);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Calculadora";
            this.TransparencyKey = System.Drawing.Color.White;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bottsuma;
        private System.Windows.Forms.Button bott7;
        private System.Windows.Forms.Button bott4;
        private System.Windows.Forms.Button bott1;
        private System.Windows.Forms.Button bott0;
        private System.Windows.Forms.Button bott2;
        private System.Windows.Forms.Button bott5;
        private System.Windows.Forms.Button bott8;
        private System.Windows.Forms.Button bottresta;
        private System.Windows.Forms.Button bottpunto;
        private System.Windows.Forms.Button bott3;
        private System.Windows.Forms.Button bott6;
        private System.Windows.Forms.Button bott9;
        private System.Windows.Forms.Button bottmult;
        private System.Windows.Forms.Button bottigual;
        private System.Windows.Forms.Button bottborrar;
        private System.Windows.Forms.Button bottrest;
        private System.Windows.Forms.Button bottdivi;
        private System.Windows.Forms.TextBox txbscreen;
        private System.Windows.Forms.Button buttsuma;
        private System.Windows.Forms.Button butt7;
        private System.Windows.Forms.Button butt4;
        private System.Windows.Forms.Button butt1;
        private System.Windows.Forms.Button butt0;
        private System.Windows.Forms.Button butt2;
        private System.Windows.Forms.Button butt5;
        private System.Windows.Forms.Button butt8;
        private System.Windows.Forms.Button buttresta;
        private System.Windows.Forms.Button buttigual;
        private System.Windows.Forms.Button buttborrar;
        private System.Windows.Forms.Button buttinicio;
        private System.Windows.Forms.Button buttdivi;
        private System.Windows.Forms.Button buttpunto;
        private System.Windows.Forms.Button butt3;
        private System.Windows.Forms.Button butt6;
        private System.Windows.Forms.Button butt9;
        private System.Windows.Forms.Button buttmult;
        private System.Windows.Forms.TextBox textscreen;
    }
}

